# CR TP GIN

Quentin Lieumont ( quentin.lieumont@telecom-paris.fr )

Dans ce TP nous utilisons NS pour simuler des connextion TCP entre différents noeuds.

## Q1

![q1](q1.png)

Nous pouvons bien voir le slow start, le congestion avoidance et le fast recovery.

Pour observer le slow start, nous allons plot de début de la courbe en echelle log.

![q1_log](q1_log.png)

On peut bien voir que $log_2$ du slow start est linéaire.
On a donc une evolution en $2^n$ pour le slow start.

On a ensuite une phase de congestion avoidance.

![q1_cong](q1_cong.png)

Comme on peut le voir sur cette courbe, on peut bien voir que la congestion avoidance n'est pas linéaire mais suit bien une forme quadratique.

![q1_end](q1_end.png)

A la fin, la fenetre ocille entre $30$ et $31$.
Cela est normal car quand la machine recoit un aquitement, elle augmente sa fenetre de $1/30$ puis si elle dépasse $31$, elle la réduit à $30$.

## Q2

Nous allons a présent étudier le fast recovery.

![q2](q2.png)

Il y a des drops de packets au bout de $29$ et $55$ secondes.

Ces drops vont remettre le slow start à $1$ et la congestion avoidance à $1/2$ de la valeur de la fenetre a laquelle les packets on drop ($20/2 = 10$).
On observe ensuite un slow start qui vient bien jusqu'à $10$ puis ensuite une phase de colision avoidance.

## Q3

![q3](q3.png)

On voit que le débit baisse lors de la seconde connection.
Or il n'y a pas de drop de packets.
Cela est du au fait que même sans changer le nombre de packets émis, leur temps d'aquitement est réduit.

Il y a alors partage de la connection entre les deux machines.

## Q4

![q4](q4.png)

On peut très bien voir que le débit TCP est réduit dès que la connexion UDP commence.
En effet, UDP est un protocole de base qui ne s'occupe pas de la congestion.
Il vas donc saturer le lien et donc la connexion TCP.

Dès que la connexion UDP s'arrette, la connexion TCP peut reprendre normalement.

## Q5

Afin de bien visualiser la connexion en régime établi, j'ai augmenté le temps de simulation à $5$ secondes.
Voici donc la fin du fichier de configuration :

```ns
# Links:
# * A-R1
$ns duplex-link $n0 $n4 100Mb 1ms DropTail
# * A'-R1
$ns duplex-link $n2 $n4 100Mb 21ms DropTail
# * B-R2
$ns duplex-link $n1 $n5 100Mb 1ms DropTail
# * B'-R2
$ns duplex-link $n3 $n5 100Mb 21ms DropTail
# * R1-R2
$ns duplex-link $n4 $n5 10Mb 4ms DropTail

$ns connect $tcp0 $tcp1
$ns connect $tcp2 $tcp3

$ns at 5.0 "finish"
$ns at 0.0 "$ftp start"
$ns at 1.0 "$ftp2 start"
$ns at 4.0 "$ftp2 stop"
$ns at 0.0 "tracepaquet"

$ns run
```

Nous obtenons les résultats suivants :

![q5](q5.png)

On peut bien voir que la connexion de 0 rest bien meilleure que celle de 1.
Logique car son lien est bien meilleur.

Du coté de la connexion totale, on voit des changments lors de l'etablissement de la seconde connexion, mais elle revient a son niveau optimal assez vite.

Du coté de la fenetre de congestion, on voit bien quelle augmente comme prévu jusqu'à $30$ mais bien plus lentement que la connexion 0 car étant donné que la connexion est plus lente, les aquitements mettent plus de temps a arriver.

On a donc un partage de la conexion au prorata de la qualité des liens.

## Q6

_Cf. Sami Tendjaoui._

- Modifiez le script paquet.tcl pour obtenir les débits moyens (unité en Mb/s) de réception du flux FTP pour des tailles maximales de la fenêtre de congestion de : 1, 5, 10, 15, 20, 25 et 30 paquets. À partir de ces valeurs tracez la courbe représentant le débit moyen (unité en Mb/s) de réception du flux FTP en fonction de la taille maximale de la fenêtre de congestion.

Nous allons scripter ce test en bash, afin de pouvoir l'effectuer 30 fois rapidement:

```bash
mkdir q6
cd q6
echo "max_cwn, debit"
for i in {1..30};
do
    sed -r "s/(set\ maxcwnd_\s*)(.+)/\1$i/" ../paquet.tcl \
    | sed "s/\(p-out-RTT\)\(\)/\1-$i/" | ns ;
    echo "$i,$(( `cat p-out-RTT-$i.cls | cut -d " " -f 2 | tail -n 1` / 5))"
done
```

En récupérant la quantité totale de paquets transmis et en la divisant par 5 (secondes), puis multipliant par 1500 octets (ie: taille d'un paquet) puis par 8 (bits), on obtient :

| Fenêtre de congestion | Débit moyen (Mb/s) |
|---------------------- | ------------------ |
| 1                     | 0.54               |
| 5                     | 2.78               |
| 10                    | 5.56               |
| 15                    | 8.87               |
| 20                    | 9.6                |
| 25                    | 9.6                |
| 30                    | 9.6                |

![Evolution](https://radosgw.rezel.net/codimd/uploads/upload_b8bb1fe8ef9786a4bcfdf559c208f474.png)

On observe que l'augmentation du maximum de la fenêtre de congestion permet d'améliorer le débit moyen jusqu'à une taille de 18 paquets. A partir de ce seuil, le débit maximal est atteint.

En effet, on observe un débit maximal de 9 Mb/s, et le débit maximal du lien est de 10 Mb/s: à partir d'une fenêtre de congestion d'une taille de 20, le lien est saturé, et il est impossible d'augmenter davantage le débit.

- Modifiez le script paquet.tcl pour obtenir les débits moyens (unité en Mb/s) de réception du flux FTP pour des délais de propagation de 2.5, 5, 15, 30 et 40 ms. À partir de ces valeurs, tracez la courbe représentant le débit moyen (unité en Mb/s) de réception du flux FTP en fonction du délai aller-et-retour.

Ici encore, nous allons automatiser cette expérience en utilisant le script bash suivant:

```bash
mkdir q62
cd q62
echo "delay, debit"
for i in `seq 2.5 0.5 40`;
do
    sed "s/\(10\)\(ms DropTail\)/$i\2/" ../paquet.tcl \
    | sed "s/\(p-out-RTT\)\(\)/\1-$i/" | ns
    echo "$i,$(( `cat p-out-RTT-$i.cls | cut -d " " -f 2 | tail -n 1` / 5))"
done
```

Nous obtenons les résultats suivants:

| Délai de propagation (ms) | Debit moyen (Mb/s) |
| ------------------------- | ------------------ |
| 2.5                       | 9.708              |
| 5                         | 9.66               |
| 15                        | 7.464              |
| 30                        | 3.72               |
| 40                        | 2.76               |

![debit](https://radosgw.rezel.net/codimd/uploads/upload_2eb47df97b0754a3669fdeedd18c777b.png)

On remarque que le débit n'est, au premier abord, pas beaucoup impacté par le temps de propagation, jusqu'à une valeur seuil d'environ 12ms au bout de laquelle le débit décroit en suivant une exponentielle décroissante.

Ce point de rupture correspond au moment où, statistiquement, une quantité non négligeable d'acquittements ne sont pas reçus avant le timeout: ainsi, la fenêtre de congestion est forcée de diminuer (Congestion Avoidance).

-----

- Dans les premières versions de TCP, la valeur recommandée pour la taille maximale de la fenêtre de congestion est de 65 ko (65535 octets). Qu'en déduisez-vous sur les performances de TCP sur un réseau FTTH par exemple (fibre optique) ? Pour cette réflexion, vous pouvez prendre l'exemple d'un destinataire situé à 1000 km et un débit à 100 Mb/s. La vitesse de propagation pouvant être fixée à 300 000 km/s). (Il est à noter que les délais de transmission peuvent être considérés comme négligeables). Que faut-il faire pour résoudre ce problème ?

On suppose que la fenêtre de congestion a atteint son maximum (ce qui arrive assez vite vu qu'on a pas de congestion)

On calcule le temps d'émission:

$$
    65 \, 535 \times 8 = 524 \, 280\ \mathrm{bits} \\
    T_e = \frac{524 \, 280}{100\times10^6} \simeq 0,5 \mathrm{ms}
$$

On calcule le délai aller-retour (on suppose que le récepteur acquitte instantanément les segments) correspondant au moment où le récepteur reçoit le premier acquittement après avoir émis.

$$
      T_{ack} = 2\times \frac{1000 \mathrm{km}}{3 \times 10^8 \mathrm{m/s}} \simeq 6,667 \mathrm{ms}
$$

Ainsi, le temps passé par l'émetteur à attendre les acquittements avant d'envoyer la suite du message est:

$$
    \Delta T = T_{ack} - T_e \simeq 6,167 \mathrm{ms} > 0
$$

L'émetteur doit donc attendre avant de pouvoir continuer la transmission.
Si on avait une valeur négative pour $\Delta T$, l'émetteur aurait reçu suffisamment d'acquittements à temps pour pouvoir transmettre de manière continue.

Ainsi, sur ce lien fibre avec un "bon" débit, l'émetteur va transmettre par salves de 65ko toutes les 6,67 ms. Le débit réel va donc être de:

$$
    D_{réel} = \frac{65\ \mathrm{ko}}{6,667\ \mathrm{ms}} = \frac{65 \, 535\ \mathrm{bits}}{6,667 \times 10^{-3}\ \mathrm{s}} \simeq 10\ \mathrm{Mbit/s}
$$

On utilise pas pleinement le débit de la fibre. C'est bien dommage!

Pour résoudre ce problème, il faudrait augmenter la taille maximale de la fenêtre de congestion.

Calculons la valeur maximale qu'il faudrait au minimum imposer à la fenêtre de congestion pour pouvoir transmettre de manière continue:

On cherche à avoir:

$$
    T_e = T_{ack}
$$
D'où:
$$
    cwnd = 100\times10^6 \times T_{ack}
$$
$$
    cwnd = 100\mathrm{Mb/s} \times 6,67 \mathrm{ms} = 667\mathrm{ko}
$$

Incroyable, pour obtenir un débit 10x supérieur, il suffit de multiplier la taille maximale de la fenêtre par 10 !
