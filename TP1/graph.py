from os import name
import pandas as pd
import plotly.express as px
import plotly.graph_objects as go


df = pd.read_csv("./f-out.cls", sep=",", header=0)
df["Debit (Mb/s)"] = df[" debit"] * 1500*8/1_024**2

print(list(df["Debit (Mb/s)"]))
fig = px.bar(df, x="max_cwn", y="Debit (Mb/s)")
fig.show()



# df["bw0"] = df.diff()["ack0"]/df.diff()["t"]
# df["bw1"] = df.diff()["ack1"]/df.diff()["t"]
# df["bw_tot"] = df.bw0 + df.bw1

# # print(df)
# # fig = px.line(df, "t", y=["bw0", "bw1"])
# # fig.show()


# data = [
#     go.Scatter(
#         x=df.t,
#         y=df.bw0,
#         yaxis="y2",
#         name="bw0"
#     ),
#     go.Scatter(
#         x=df.t,
#         y=df.bw1,
#         yaxis="y2",
#         name="bw1"
#     ),
#     go.Scatter(
#         x=df.t,
#         y=df.bw_tot,
#         yaxis="y2",
#         name="bw total"
#     ),
#     go.Scatter(
#         x=df.t,
#         y=df.crwd0,
#         name="crwd 0"
#     ),
#     go.Scatter(
#         x=df.t,
#         y=df.crwd1,
#         name="crwd 1"
#     )
# ]

# l = 0.4

# layout = go.Layout(
#     yaxis=dict(
#         domain=[0, l]
#     ),
#     legend=dict(
#         traceorder="reversed"
#     ),
#     yaxis2=dict(
#         domain=[l, 1]
#     )
# )
# fig = go.Figure(data=data, layout=layout)
# fig.show()


# # df["bw_TCP"] = df.diff()["TCP"]/df.diff()["t"]
# # df["bw_UDP"] = df.diff()["UDP"]/df.diff()["t"]


# # fig = px.line(df, "t", y=["bw_TCP", "bw_UDP"])
# # fig.show()